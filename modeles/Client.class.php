<?php
class Client
{
    private $id;
    private $nom;
    private $prenom;
    private $mail;
    private $numero;
    private $mdp;
    private $adresse;
    private $codePostal;
    private $pays;


    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of nom
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of prenom
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set the value of prenom
     *
     * @return  self
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get the value of mail
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set the value of mail
     *
     * @return  self
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get the value of numero
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set the value of numero
     *
     * @return  self
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }



    /**
     * Get the value of adresse
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set the value of adresse
     *
     * @return  self
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get the value of codePostal
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set the value of codePostal
     *
     * @return  self
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get the value of pays
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set the value of pays
     *
     * @return  self
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get the value of mdp
     */
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * Set the value of mdp
     *
     * @return  self
     */
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;

        return $this;
    }

    public static function verifier($mail, $mdp)
    {
        $mdp = hash("sha256", $mdp);
        $req = MonPdo::getInstance()->prepare("select * from client where mail =:mail and mdp=:mdp");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'client');
        $req->bindParam('mail', $mail);
        $req->bindParam('mdp', $mdp);
        $req->execute();
        $leResultat = $req->fetch();


        if ($leResultat) {
            $rep = $leResultat;
        } else {
            $rep = false;
        }

        return $rep;
    }

    public function hydrate($nom, $prenom, $mail, $numero, $mdp, $adresse, $codePostal, $pays)
    {
        $this->setNom($nom);
        $this->setPrenom($prenom);
        $this->setMail($mail);
        $this->setNumero($numero);
        $this->setMdp($mdp);
        $this->setAdresse($adresse);
        $this->setCodePostal($codePostal);
        $this->setPays($pays);
    }


    public static function creationCompteClient(Client $client)
    {
        try {
            $req = MonPdo::getInstance()->prepare("insert into client(nom, prenom, mail, numero, mdp, adresse, code_postal, pays) values(:nom, :prenom, :mail, :numero, :mdp, :adresse, :codePostal, :pays)");
            $req->bindValue(':nom', $client->getNom());
            $req->bindValue(':prenom', $client->getPrenom());
            $req->bindValue(':mail', $client->getMail());
            $req->bindValue(':numero', $client->getNumero());
            $req->bindValue(':mdp', hash("sha256", $client->getMdp()));
            $req->bindValue(':adresse', $client->getAdresse());
            $req->bindValue(':codePostal', $client->getCodePostal());
            $req->bindValue(':pays', $client->getPays());

            $req->execute();
        } catch (PDOException $e) {

            echo $e->getMessage();
        }
    }

    public static function deconnexion()
    {
        if (isset($_SESSION["autorisationClient"])) {
            unset($_SESSION["autorisationClient"]);
            unset($_SESSION["nom"]);
            unset($_SESSION["prenom"]);
            unset($_SESSION["mail"]);
            unset($_SESSION["numero"]);
            unset($_SESSION["adresse"]);
            unset($_SESSION["code_postal"]);
            unset($_SESSION["pays"]);
        }
    }
}
