<?php
class Categorie
{
    private $id;
    private $nomCat;

    function getId()
    {
        return $this->id;
    }



    public static function afficherTous()
    {

        $req = MonPdo::getInstance()->prepare("select * from categorie");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'categorie');
        $req->execute();
        $lesResulats = $req->fetchAll();
        return $lesResulats;
    }

    public static function trouverUneCategorie($id)
    {
        $req = MonPdo::getInstance()->prepare("select * from categorie where id=:id");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'categorie');
        $req->bindParam('id', $id);
        $req->execute();
        $leResultat = $req->fetch();
        return $leResultat;
    }

    /**
     * Get the value of nomCat
     */
    public function getNomCat()
    {
        return $this->nomCat;
    }

    /**
     * Set the value of nomCat
     *
     * @return  self
     */
    public function setNomCat($nomCat)
    {
        $this->nomCat = $nomCat;

        return $this;
    }
}
