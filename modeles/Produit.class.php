<?php

class Produit
{
    private $id;
    private $nom;
    private $prix;
    private $photo;
    private $categorie;
    private $description;

    public function hydrate($nom, $prix, $categorie, $photo, $description)
    {
        $this->setNom($nom);
        $this->setPrix($prix);
        $this->setDescription($description);
        $this->setPhoto($photo);
        $this->setCategorie($categorie);
    }

    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * Get the value of photo
     */
    public function getPhoto()
    {


        return $this->photo;
    }

    /**
     * Get the value of nom
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of prix
     */
    public function getPrix()
    {
        return $this->prix;
    }

    public static function afficherTous()
    {
        $req = MonPdo::getInstance()->prepare("select produit.id, nom, description, prix, photo, nomCat from produit inner join categorie on categorie.id=produit.categorie;");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'produit');
        $req->execute();
        $lesResultats = $req->fetchAll();
        return $lesResultats;
    }

    public static function afficherParCategorie($categorieID)
    {
        $req = MonPdo::getInstance()->prepare("select produit.id, nom, description, prix, photo, nomCat from produit inner join categorie on categorie.id=produit.categorie where produit.categorie=:categorie;");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'produit');
        $req->bindValue(":categorie", $categorieID);
        $req->execute();
        $lesResultats = $req->fetchAll();
        return $lesResultats;
    }

    function getIdCat()
    {
        return Categorie::trouverUneCategorie($this->id);
    }

    public static function recherche($unNom)
    {
        $req = MonPdo::getInstance()->prepare("select * from produit where nom like :unNom");
        $req->bindValue(":unNom", "%$unNom%");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'produit');
        $req->execute();
        $lesResultats = $req->fetchAll();
        return $lesResultats;
    }

    public static function ajouter(Produit $produit)
    {
        try {
            $req = MonPdo::getInstance()->prepare("insert into produit(nom, description, prix, photo,categorie) values(:nom, :description, :prix, :photo,:categorie)");
            $req->bindValue('nom', $produit->getNom());
            $req->bindValue('description', $produit->getDescription());
            $req->bindValue('prix', $produit->getPrix());
            $req->bindValue('photo', $produit->getPhoto());
            $req->bindValue('categorie', $produit->getCategorie());

            $nb = $req->execute();
            return $nb;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function modifier(Produit $produit)
    {
        $nom = filter_var(securiser($_POST['nom']), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $description = filter_var(securiser($_POST['description']), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $prix = filter_var(securiser($_POST["prix"]), FILTER_SANITIZE_NUMBER_FLOAT);
        $categorie = filter_var(securiser($_POST["categorie"]), FILTER_SANITIZE_NUMBER_INT);

        $token = filter_var(securiser($_POST["token"]), FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        if ($token == $_SESSION["token"]) {

            try {
                $req = MonPdo::getInstance()->prepare("UPDATE produit SET nom = '$nom', 
                description = '$description', prix =$prix ,  categorie = '$categorie'  WHERE id = $_GET[id]");

                $nb = $req->execute();
                return $nb;
            } catch (PDOException $e) {

                echo $e->getMessage();
            }
        }
    }



    public static function trouverUnProduit($id)
    {
        $req = MonPdo::getInstance()->prepare("select * from produit where id=:id");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'produit');
        $req->bindParam('id', $id);
        $req->execute();
        $leResultat = $req->fetch();
        return $leResultat;
    }

    public static function supprimerUnProduit($id)
    {
        try {


            $req = MonPdo::getInstance()->prepare("delete from produit WHERE id = $id");

            $nb = $req->execute();

            return $nb;
        } catch (PDOException $e) {

            echo $e->getMessage();
        }
    }

    public static function AjoutPanier($id)
    {
        // var_dump($_SESSION['panier']);

        if (!isset($_SESSION['panier'])) {
            $_SESSION['panier'] = array(); //création d'une variable de cession
        }

        if (isset($_SESSION['panier'][$id])) {
            $_SESSION['panier'][$id]++; //ajoute 1 à la quantité
        } else {
            $_SESSION['panier'][$id] = 1; //sinon met un dans la quantité
        }

        $_SESSION['succes'] = "le produit a été ajouté au panier !";
    }

    public static function AjoutPanier2($id)
    {

        $_SESSION['panier'][$id]++;
    }

    public static function SupprimerPanier()
    {
        unset($_SESSION['panier']);
    }
    public static function retraitPanier2($id)
    {

        if ($_SESSION['panier'][$id] == 1) {
            unset($_SESSION['panier'][$id]);
            // header("location:panier.php");
        } else {
            $_SESSION['panier'][$id]--; //enleve 1 à la quantité
        }
    }

    public static function AfficherPanier()
    {
        $arrayResultat = [];
        foreach ($_SESSION['panier'] as $id) {

            $req = MonPdo::getInstance()->prepare("select * from produit where id=$id");
            $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'produit');
            $req->execute();
            $leResultat = $req->fetch();
            array_push($arrayResultat, $leResultat);
        }
        return $arrayResultat;
    }

    /**
     * Get the value of description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }



    /**
     * Get the value of categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set the value of categorie
     *
     * @return  self
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }
}
