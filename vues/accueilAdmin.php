<?php ob_start();

if (isset($_SESSION['modif'])) {
?>
    <div class="alert alert-success" role="alert" id="alert">
        <?php echo $_SESSION['modif'];
        unset($_SESSION["modif"]);
        ?>
    </div>


<?php   }
if (isset($_SESSION['succes'])) {
?>
    <div class="alert alert-success" role="alert" id="alert">
        <?php echo $_SESSION['succes'];
        unset($_SESSION["succes"]);
        ?>
    </div>
<?php
}
?>



<div style="color : #FFD700 " class="container">
    <h1>Page admin</h1>
    <div class="row">
        <a href="index.php?uc=produit&action=formAjout" class="btn btn-success">
            Ajouter un produit
        </a>
    </div>
    <?php

    if (isset($_SESSION['succes'])) {
    ?>
        <div class="alert alert-success" role="alert" id="alert">
            <?php echo $_SESSION['succes'];
            unset($_SESSION["succes"]);
            ?>
        </div>
    <?php
    }

    ?>
    <div class="row">
        <table class="table">
            <theader>
                <tr style="color : #FFD700 ">
                    <th>Id</th>
                    <th>Nom</th>
                    <th>Prix</th>
                    <th>Categorie</th>
                    <th>Actions</th>
                </tr>
            </theader>
            <tbody>
                <?php
                foreach ($lesProduits as $unProduit) {

                ?>
                    <tr style="color : white ">

                        <td><?= $unProduit->getId() ?></td>
                        <td><?= $unProduit->getNom() ?></td>
                        <td><?= $unProduit->getPrix() ?>€</td>
                        <td><?= $unProduit->getCategorie() ?></td>
                        <td>
                            <a class="btn btn-primary" href="index.php?uc=produit&action=formModif&modif=<?php echo $unProduit->getId() ?>"> Modifier</a>

                            <a class="btn btn-danger" href="index.php?uc=produit&action=confirmerSuppression&modif=<?php echo $unProduit->getId() ?>">Suprimer</button>

                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php
$content = ob_get_clean();
include("template.php");
?>