<?php ob_start(); ?>
<div class="container-fluid ketchup">
    <div class="card mb-3" style="max-width: 600px; max-height: 400px">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="images/accueil.png" alt="img1" width="100%" height="100%" class="img_slider active imageAjoutClient " />
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">Modifier un produit :</h5>
                    <form class="d-flex" action="index.php?uc=produit&action=traitementModif&id=<?php echo $_GET["modif"] ?>" method="POST">
                        <div class="mb-3">
                            <label class="form-label">Nom du produit</label>
                            <input type="text" name="nom" value="<?php echo $unProduit->getNom() ?>"> <br>
                            <label class="form-label">Description</label>
                            <input type="text" name="description" value="<?php echo $unProduit->getDescription() ?>"><br>
                            <label class="form-label">Prix</label>
                            <input type="text" name="prix" value="<?php echo $unProduit->getPrix() ?>"><br>
                            <label class="form-label">Categorie</label>
                            <input type="text" name="categorie" value="<?php echo $unProduit->getCategorie() ?>"><br>
                            <input id="secuCSRF" name="token" type="hidden" value=<?php echo $_SESSION["token"] ?>>
                            <input type="submit" name="valider" value="valider">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$content = ob_get_clean();
include("template.php");
?>