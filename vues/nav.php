<nav id="navBar" class="navbar navbar-expand-lg text-center sticky-top d-flex justify-content-space-evenly">
    <a style="color : #FFD700" href="index.php" id="iconeTasse" class="navbar-brand"><i class="fa fa-coffee"></i></a>

    <!-- juste pour pouvoir visiter les autres pages du site pendant la phase test -->
    <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle dropdown-boutique" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Boutique
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="index.php?uc=produit&action=liste">Toutes les catégories</a>
            <a class="dropdown-item" href="index.php?uc=produit&action=liste&categorieID=1">Espresso</a>
            <a class="dropdown-item" href="index.php?uc=produit&action=liste&categorieID=2">Lungo</a>
            <a class="dropdown-item" href="index.php?uc=produit&action=liste&categorieID=3">Ristretto</a>
            <a class="dropdown-item" href="index.php?uc=produit&action=liste&categorieID=4">Barista</a>

        </div>
    </div>

    <?php if (!isset($_SESSION["autorisationClient"])) { ?>
        <li class="nav-item list-unstyled">
            <a class="nav-link" href="index.php?uc=clients&action=formulaireConnexion" style="color : #FFD700">Se connecter</a>
        </li>

        <li class="nav-item list-unstyled">
            <a class="nav-link" href="index.php?uc=clients&action=formulaireCreation" style="color : #FFD700">Créer un compte</a>
        </li>
    <?php
    } ?>
    <li class="nav-item list-unstyled">
        <a class="nav-link" href="index.php?uc=clients&action=formulaireContact" style="color : #FFD700">Nous contacter</a>
    </li>
    <?php

    if (isset($_SESSION["autorisationClient"]) && $_SESSION["autorisationClient"] == "OK") { ?>
        <li class="nav-item list-unstyled">
            <a class="nav-link" style="color : #FFD700" href="index.php?uc=clients&action=deconnexion">Se déconnecter</a>
        </li>

    <?php }
    ?>



    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a id="icone2" class="nav-link" href="index.php?uc=produit&action=afficherPanier"><i style="color:#CEAD4F;" class="fas fa-shopping-cart fa-2x"></i></a>
                    <span class="badge badg-primary" style="color : #FFD700" ;"><?php if (isset($_SESSION['panier'])) {
                                                                                    echo count($_SESSION['panier'], COUNT_RECURSIVE);
                                                                                } ?></span>
                </li>
            </ul>



            <form class="form-inline my-2 my-lg-0" method="POST" action="index.php?uc=produit&action=recherche">
                <input class=" form-control mr-sm-2" style="color:white" type="search" placeholder="Rechercher" aria-label="Search" name="recherche">
                <button id="boutonRecherche" class="btn btn-outline-success my-2 my-sm-0 " type="submit"><i class="fas fa-search"></i></button>
            </form>
        </ul>

        <?php if (!isset($_SESSION["autorisation"])) { ?>
            <li class="nav-item  list-unstyled">
                <a class="nav-link partie-admin" style="color : #FFD700" href="index.php?uc=admin&action=formulaire">Partie admin</a>
            </li>
        <?php
        }
        if (isset($_SESSION["autorisation"]) && $_SESSION["autorisation"] == "OK") { ?>
            <li class="nav-item list-unstyled">
                <a class="nav-link list-unstyled partie-admin" href="index.php?uc=admin&action=deconnexion">Se déconnecter</a>
            </li>
        <?php }

        if (isset($_SESSION["autorisation"]) && $_SESSION["autorisation"] == "OK") {  ?>
            <li class="nav-item list-unstyled partie-admin">
                <a class="nav-link " style="color : #FFD700" href="index.php?uc=admin&action=espaceAdmin">espace admin</a>
            </li>
        <?php }
        ?>


    </div>
</nav>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script> -->