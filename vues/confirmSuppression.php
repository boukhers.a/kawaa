<?php ob_start();
?>
<div class="container container-fluid d-flex justify-content-center align-items-center container-confirm-supp">
    <div class="card card-pastouche mb-3">
        <div class="row g-0">
            <div class="col-md-5">
                <img src="images/accueil.png" width="100%;" height="100%;" alt="img1" class="imageAjoutClient">
            </div>
            <div class="col-md-7">
                <div class="card-body" style=" background-color : #FFD700;">
                    <h5 class="card-title">Voulez vous vraiment supprimer le produit suivant ?</h5>
                    <p class="card-text">
                    <form class="d-block" action="index.php?uc=produit&action=traitementSupprimer&id=<?php echo $_GET["modif"] ?>" method="POST">
                        <div>
                            <strong>id :</strong> <?php echo $_GET["modif"] ?> <br>
                            <strong>Nom :</strong> <?php echo $unProduit->getNom() ?> ;<br>
                            <strong>Description :</strong> <?php echo $unProduit->getDescription() ?> ;<br>
                            <strong>Prix :</strong> <?php echo $unProduit->getPrix() ?> ;<br>
                            <strong>id Catégorie :</strong> <?php echo $unProduit->getCategorie() ?> ;
                        </div>
                        <input type="submit" name="valider" value="valider" class="btn btn-prout">
                    </form>
                    </p>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
$content = ob_get_clean();
include("template.php");
?>