<?php ob_start(); ?>
<div class="container-fluid">
    <div class="card mb-3">
        <div class="row g-0">
            <div class="col-md-6">
                <img src="images/slider1.jpg" alt="img1" width="100%" height="65.6%" class="img_slider active imageAjoutClient " />
            </div>
            <div class="col-md-6">
                <div class="card-body">
                    <h5 class="card-title">Créer un compte client :</h5>
                    <form action="index.php?uc=clients&action=creationCompte" method="post">
                        <!-- <?=
                                isset($_SESSION["error"]) ? '<div class="alert alert-danger">' . $_SESSION["error"] . '</div>' : "" ?> -->
                        <div style="background-color : #FFD700" class="row mb-2">
                            <label for="inputEmail">Nom </label>
                            <input name="nom" type="text" required minlength="1" required class="form-control">
                        </div>
                        <div style="background-color : #FFD700" class="row mb-2">
                            <label for="inputMdp">Prenom </label>
                            <input name="prenom" type="text" required minlength="1" class="form-control">
                        </div>
                        <div style="background-color : #FFD700" class="row mb-2">
                            <label for="inputMdp">Mail </label>
                            <input name="mail" type="email" required minlength="6" class="form-control">
                        </div>
                        <div style="background-color : #FFD700" class="row mb-2">
                            <label for="inputMdp">Numero de téléphone </label>
                            <input name="numero" type="string" required minlength="10" class="form-control">
                        </div>
                        <div style="background-color : #FFD700" class="row mb-2">
                            <label for="inputMdp">Mot de passe </label>
                            <input name="mdp" type="password" required minlength="8" class="form-control">
                        </div>
                        <div style="background-color : #FFD700" class="row mb-2">
                            <label for="inputMdp">Adresse </label>
                            <input name="adresse" type="text" required minlength="10" class="form-control">
                        </div>
                        <div style="background-color : #FFD700" class="row mb-2">
                            <label for="inputMdp">Code Postal </label>
                            <input name="codePostal" type="text" required minlength="5" class="form-control">
                        </div>
                        <div style="background-color : #FFD700" class="row mb-2">
                            <label for="inputMdp">Pays </label>
                            <input name="pays" type="text" required minlength="5" class="form-control">
                        </div>
                        <input id="secuCSRF" name="token" type="hidden" value=<?php echo $_SESSION["token"] ?>>
                        <button class="btn btn-primary " id="submitButton" type="submit">Envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$content = ob_get_clean();
include("template.php");
?>