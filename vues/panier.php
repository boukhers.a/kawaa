<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title> DKR-Café</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Gentium+Book+Plus:ital@1&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style/style.css">
</head>

<?php
include("nav.php");
?>

<body>

    <div class="container container-fluid">
        <div id="ui-view" data-select2-id="ui-view">
            <div>
                <div class="card">
                    <div class="card-header">Votre panier d'achat

                        <a class="btn btn-sm btn-secondary float-right mr-1 d-print-none" href="#" onclick="javascript:window.print();" data-abc="true">
                            <i class="fa fa-print"></i> imprimer</a>
                        <span class="badge bg-primary"><?php if (isset($_SESSION['panier'])) {
                                                            echo count($_SESSION['panier'], COUNT_RECURSIVE);
                                                        }  ?> variétés commandées ! </span>
                        <a class="btn btn-sm btn-info float-right mr-1 d-print-none" href="index.php?uc=produit&action=supprimerPanier" data-abc="true">
                            <i class="fa fa-save"></i> Vider le panier</a>



                    </div>
                    <div class="card-body">
                        <div class="row mb-4">
                            <div class="col-sm-4">
                                <h6 class="mb-3">Expéditeur</h6>
                                <div>
                                    <strong>DKR-Kafé</strong>
                                </div>
                                <div>77, rue de la Liberté</div>
                                <div>Dakar, Sénégal</div>
                                <div>DKRKafe@gmail.com</div>
                                <div> +221 7 81 43 58 56</div>
                            </div>
                            <div class="col-sm-4">
                                <h6 class="mb-3"></h6>
                                <div>
                                    <strong><?php  ?></strong>
                                </div>
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                            <div class="col-sm-4">
                                <h6 class="mb-3"><?php echo date('d/m/y') ?></h6>
                                <div>Nom :
                                    <strong><?php if (isset($_SESSION['nom'])) {
                                                echo $_SESSION["nom"];
                                            } ?> <?php if (isset($_SESSION['prenom'])) {
                                                        echo $_SESSION["prenom"];
                                                    } ?></strong>
                                </div>
                                <div></div>
                                <div><?php if (isset($_SESSION['numero'])) {
                                            echo $_SESSION["numero"];
                                        } ?></div>
                                <div><?php if (isset($_SESSION['adresse'])) {
                                            echo $_SESSION["adresse"];
                                        } ?></div>
                                <div><?php if (isset($_SESSION['code_postal'])) {
                                            echo $_SESSION["code_postal"];
                                        } ?> - <?php if (isset($_SESSION['pays'])) {
                                                    echo $_SESSION["pays"];
                                                } ?></div>
                            </div>
                        </div>
                        <div class="table-responsive-sm">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="center">id</th>
                                        <th>Produit</th>
                                        <th>Enlever/Ajouter</th>
                                        <th class="center">Quantité</th>
                                        <th class="right">Prix unitaire</th>
                                        <th class="right">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    // var_dump($_SESSION['panier']);
                                    if (!empty($_SESSION['panier'])) {
                                        // require "config.php";
                                        //         $bdd = connect();
                                        $prixTotalHt = 0;
                                        $nombredarticle = 0;

                                        foreach ($_SESSION['panier'] as $id => $quantite) {
                                            $sql = "select * from produit where id='$id=>$quantite'";
                                            $resultat = MonPDO::getInstance()->query($sql);


                                            //stockage resultat dans un tableau
                                            while ($produit = $resultat->fetch(PDO::FETCH_OBJ)) {

                                                $prixTotalHt = $prixTotalHt + ($produit->prix * $quantite); ?>
                                                <tr>
                                                    <td class="center">1</td>
                                                    <td class="left"><?php echo $produit->nom ?></td>
                                                    <td class="left"><a href="index.php?uc=produit&action=ajouterPanier2&id=<?php echo $id ?>" button type="button" class="btn btn-warning">+</button>
                                                            <a href="index.php?uc=produit&action=retraitPanier2&id=<?php echo $id ?>" button type="button" class="btn btn-warning">-</button></td>
                                                    <td class="center"><?php echo $quantite ?></td>
                                                    <td class="right"><?php echo $produit->prix ?></td>
                                                    <td class="right"><?php echo $quantite * $produit->prix ?></td>
                                                </tr>
                                        <?php
                                            }
                                        } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-sm-5"></div>
                            <div class="col-lg-4 col-sm-5 ml-auto">
                                <table class="table table-clear">
                                    <tbody>
                                        <tr>
                                            <td class="left">
                                                <strong>Prix Total H.T</strong>
                                            </td>
                                            <td class="right"><?php echo $prixTotalHt ?></td>
                                        </tr>
                                        <tr>
                                            <td class="left">
                                                <strong>Remise 5%</strong>
                                            </td>
                                            <td class="right"><?php $prixTotalHt = $prixTotalHt * 0.95;
                                                                echo round($prixTotalHt, 2) ?></td>
                                        </tr>
                                        <tr>
                                            <td class="left">
                                                <strong>T.V.A (10%)</strong>
                                            </td>
                                            <td class="right"><?php $TVA = $prixTotalHt * 0.10;
                                                                echo round($TVA, 2) ?></td>
                                        </tr>
                                        <tr>
                                            <td class="left">
                                                <strong>Frais de port</strong>
                                            </td>
                                            <td class="right"><?php $fraisDePort = 5;
                                                                echo round($fraisDePort, 2) ?></td>
                                        </tr>
                                        <tr>
                                            <td class="left">
                                                <strong>Prix Total T.T.C</strong>
                                            </td>
                                            <td class="right">
                                                <strong> <?php $prixTotalTtc = $prixTotalHt + $TVA + $fraisDePort;
                                                            echo round($prixTotalTtc, 2) ?> </strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<?php } ?>
<script src="JS/app.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>


</body>





</html>