<?php ob_start();
if (isset($_SESSION['succes'])) {
?>
    <div class="alert alert-success" role="alert" id="alert">
        <?php echo $_SESSION['succes'];
        unset($_SESSION["succes"]);
        ?>
    </div>
<?php
}
?>

<div class="container container-fluid ketchup">
    <div class="card mb-3" style="max-width: 900px; max-height: 500px ">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="images/accueil.png" width="100%" height="100%" alt="img1" />
            </div>
            <div class="col-md-8">
                <div class="card-body" style=" background-color : #FFD700;">
                    <h5 class="card-title">Nous contacter : </h5>
                    (réponse sous 48h jours ouvrables)
                    <p class="card-text">
                    <form method="post" id="contactForm" action="index.php?uc=clients&action=contact">

                        <!-- Name input -->
                        <div class="mb-3">
                            <label class="form-label" for="name">Nom</label>
                            <input class="form-control" name="name" type="text" placeholder="Nom" data-sb-validations="required" />
                            <div class="invalid-feedback" data-sb-feedback="name:required">Veuillez saisir votre nom</div>
                        </div>

                        <!-- Email address input -->
                        <div class="mb-3">
                            <label class="form-label" for="email">Adresse email</label>
                            <input class="form-control" name="email" type="email" placeholder="Email Address" data-sb-validations="required, email" />
                            <div class="invalid-feedback" data-sb-feedback="email:required">Veuillez saisir votre email</div>
                            <div class="invalid-feedback" data-sb-feedback="email:email">Veuillez saisir un email valide</div>
                        </div>

                        <!-- Message input -->
                        <div class="mb-3">
                            <label class="form-label" for="message">Message</label>
                            <textarea class="form-control" name="message" type="text" placeholder="Message" style="height: 10rem;" data-sb-validations="required"></textarea>
                            <div class="invalid-feedback" data-sb-feedback="message:required">Veuillez saisir votre message</div>
                        </div>

                        <!-- Form submissions success message -->
                        <div class="d-none" id="submitSuccessMessage">
                            <div class="text-center mb-3">Votre message a bien été transmis !</div>
                        </div>

                        <!-- Form submissions error message -->
                        <div class="d-none" id="submitErrorMessage">
                            <div class="text-center text-danger mb-3">Erreur lors de la transmission du message, veuillez réessayer</div>
                        </div>

                        <!-- Form submit button -->
                        <div class="d-grid">
                            <button class="btn btn-primary " id="submitButton" type="submit">Envoyer</button>
                        </div>
                        <input id="secuCSRF" name="token" type="hidden" value=<?php echo $_SESSION["token"] ?>>
                    </form>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$content = ob_get_clean();
include("template.php");
?>