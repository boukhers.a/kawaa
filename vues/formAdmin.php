<?php ob_start(); ?>


<div class="container container-fluid ketchup" style="background-color : #FFD700">
    <div class="row col-lg-3 col-md-4 col-sm-6 col-12 mx-auto d-flex justify-content-space-evenly" style="max-width: 500px; max-height: 400px ">
        <h1>Se connecter</h1>
        <form action="index.php?uc=admin&action=verif" method="post">

            <div class="row mb-2">
                <label for="inputEmail">Login* </label>
                <input name="login" id="inputEmail" type="text" required class="form-control">
            </div>
            <div class="row mb-2">
                <label for="inputMdp">MDP* </label>
                <input name="mdp" id="inputMdp" type="password" required minlength="4" class="form-control">
            </div>
            <input id="secuCSRF" name="token" type="hidden" value=<?php echo $_SESSION["token"] ?>>

            <button class="btn btn-danger">Envoyer</button>
        </form>

    </div>
</div>

<?php
$content = ob_get_clean();
include("template.php");
?>