<?php ob_start();

?>


<div class="container container-fluid d-flex justify-content-center align-items-center" style="height:36.7rem;">
    <div class="row m-4 py-3">

        <div class="col col-md-6 ">
            <img src="images/accueil.png" style="height:100%; width:100%; border-radius:13px;" alt="img1">
        </div>
        <div class="col col-md-6 blanche d-flex justify-content-center align-item-center">
            <form class="form-ajout-property" action="index.php?uc=produit&action=traitementAjout" method="POST" enctype="multipart/form-data">
                <label class="form-label d-block">Nom du produit :</label>
                <input type="text" name="nom" class="d-block rounded">
                <label class="form-label d-block">Description du produit :</label>
                <input type="text" name="description" class="d-block rounded">
                <label class="form-label d-block">Prix :</label>
                <input type="text" name="prix" class="d-block rounded">
                <label class="form-label d-block">Categorie :</label>
                <input type="text" name="categorie" class="d-block rounded">
                <label class="form-label d-block">Image :</label>
                <input type="file" name="img" class="btn btn-formulaire-ajout d-block mt-3">
                <input id="secuCSRF" name="token" type="hidden" value=<?php echo $_SESSION["token"] ?>>
                <input type="submit" name="valider" value="valider" class="btn btn-formulaire-ajout d-block mt-3">
            </form>
        </div>
    </div>
</div>
<?php
$content = ob_get_clean();
include("template.php");
?>