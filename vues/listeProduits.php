<?php
ob_start();

if (isset($_SESSION['succes'])) {
?>
    <div class="alert alert-success" role="alert" id="alert">
        <?php echo $_SESSION['succes'];
        unset($_SESSION["succes"]);
        ?>
    </div>

<?php
}

?>

<div class="row row-fluid m-0 d-flex justify-content-center">
    <?php
    // ici on justifie le contenu de maniere a obtenir un rendu équilibré grâce au d-flex + justify-content-space-evenly
    foreach ($lesProduits as $produit) {
        echo "<div class='card text-center mx-auto d-flex justify-content-space-evenly'  style='width: 20rem;'>";
        echo    "<img class='card-img-top' src='images/" . $produit->getPhoto() . "' >";
        echo    "<div  class='card-body   '>";
        echo    "<h5 class='card-title'>" . $produit->getNom() . "</h5>";
        echo    "<p class='card-prix'> " . $produit->getPrix() . " €</p>";
        echo   "<p class='card-text' >" . $produit->getDescription() . "</p>";
        echo    "</div>";
        echo "<div class='container-fluid my-3'>";
        echo "<a href='index.php?uc=produit&action=ajoutPanier&id=" . $produit->getId() . " ' class='btn btn-prout' 
        
        >Ajouter au <i class='fas fa-cart-plus fa-2x'></i></a>";
        echo    "</div>";
        echo    "</div>";
    }
    ?>
</div>

<?php
$content = ob_get_clean();
include("template.php");
?>