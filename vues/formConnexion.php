<?php ob_start(); ?>
<div class="container container-fluid ketchup">
    <div class="card" style="max-height: 500px;">
        <div class="card-body" style="background-color : #FFD700">
            <form style="background-color : #FFD700" action="index.php?uc=clients&action=verif" method="post">

                <div style="background-color : #FFD700" class="row mb-2" style="color : white">
                    <label for="inputEmail">Votre e-mail </label>
                    <input name="mail" type="email" required class="form-control">
                </div>
                <div style="background-color : #FFD700" class="row mb-2">
                    <label for="inputMdp">Mot de passe </label>
                    <input name="mdp" type="password" required minlength="4" class="form-control">
                </div>
                <input id="secuCSRF" name="token" type="hidden" value=<?php echo $_SESSION["token"] ?>>
                <button id="boutton" class="btn btn" style="background-color:white" style="color:white">Envoyer</button>
            </form>
        </div>
        <div class="imageForm" style="max-height: 300px">
            <img src="images/accueil.png" alt="img1" class="img_slider active img-fluid" />
        </div>
    </div>
</div>
<?php
$content = ob_get_clean();
include("template.php");
?>