<?php ob_start();

if (isset($_SESSION['creationCompte'])) {
?>
    <div class="alert alert-success" role="alert" id="alert">
        <?php echo $_SESSION['creationCompte'];
        unset($_SESSION["creationCompte"]);
        ?>
    </div>
<?php
}


?>

<div class="container container-fluid ketchup">
    <div class="card" style="max-width: 1400px; max-height: 500px;  background-color : #FFD700; ">
        <div class=" slider">
            <img src="images/accueil.png" alt="img1" class="img_slider active" />
            <img src="images/slider1.jpg" alt="img2" class="img_slider" />
            <img src="images/slider2.jpg" alt="img3" class="img_slider" />
            <div class="suivant">
                <i class="fa-solid fa-angle-right"></i>
            </div>
            <div class="precedent">
                <i class="fa-solid fa-angle-left"></i>
            </div>
        </div>
        <div class="card-body" style="background-color : #FFD700;">
            <p class="card-text" style="max-height: 100em; ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        </div>
    </div>
</div>

<?php
$content = ob_get_clean();
include("template.php");
?>