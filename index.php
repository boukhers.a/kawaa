<?php
session_start();

//Si le token est dans la session je le garde sinon on en genere un nouveau
$_SESSION["token"] = $_SESSION["token"] ?? bin2hex(random_bytes(32));

// var_dump(getCodePostal());

function securiser($donnees)
{
    //Efface les espaces en début et fin de chaine
    $donnees = trim($donnees);
    //Enleve les antislashes pour evité d'ignorer du code
    $donnees = stripslashes($donnees);
    //Empeche l'interpretation de balise HTML
    $donnees = htmlspecialchars($donnees);
    return $donnees;
}

spl_autoload_register(function ($class_name) {

    include "modeles/" . $class_name . ".class.php";
});



if (empty($_GET["uc"])) {
    $uc = "accueil";
} else {
    $uc = securiser($_GET["uc"]);
}

switch ($uc) {
    case "accueil":
        include("vues/accueil.php");
        break;
    case "produit":
        include("controleurs/controleurProduit.php");
        break;
    case "admin":
        include("controleurs/controleurAdmin.php");
        break;
    case "clients":
        include("controleurs/controleurClient.php");
        break;
}
