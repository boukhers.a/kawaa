<?php

use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\Exception;
// use PHPMailer\PHPMailer\SMTP;

$action = $_GET["action"];


switch ($action) {
    case "contact":
        require 'PHPMailer-master/src/Exception.php';
        require 'PHPMailer-master/src/PHPMailer.php';
        require 'PHPMailer-master/src/SMTP.php';

        $name = filter_var(securiser($_POST['name']), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $message = filter_var(securiser($_POST['message']), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $email = filter_var(securiser($_POST["email"]), FILTER_SANITIZE_EMAIL);
        $token = filter_var(securiser($_POST["token"]), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        if ($token == $_SESSION["token"]) {
            //Vérification que l'adresse mail envoyer dans le form est valide
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $emailErr = "Invalid email format";
            }
            //création d'un objet PHPmailer
            $mail = new PHPmailer();
            //on utilise un serveur mail SMTP pour envoyer le mail
            $mail->IsSMTP();
            //on designe l'adresse du serveur smtp
            $mail->Host = 'smtp.gmail.com';
            //le port du conexion du serveur smtp
            $mail->Port = 465;
            //le mode encryption 
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            //On a pas de certificat SSL alors il faut que l'on desactive
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            //l'authentification au serveur se fait pas smtp
            $mail->SMTPAuth = true;
            //on précise que le smtp dispose d'un ssl
            $mail->SMTPSecure = 'ssl';
            //tes identifiant gmail
            $mail->Username = "boukhers.a@gmail.com"; //votre gmail
            $mail->Password = "xetcerkkqkjyqmjl"; // mdp gmail

            //la provenance du mail
            $mail->setFrom('boukhers.a@gmail.com', 'Formulaire de contact'); // votre gmail
            //les destinataire du mail (note j'ai ajouter le deuxième destinataire)
            $mail->AddAddress($email);
            $mail->AddAddress('boukhers.a@gmail.com');

            //Le corps du mail est ecrit en HTML
            $mail->IsHTML(true);
            //Le sujet du mail 
            $mail->Subject = 'message de ' . $name . ' email ' . $email;
            //le corps du mail 
            $mail->Body = $message;
            //l'encodage des char dans le mail
            $mail->CharSet = "UTF-8";
            //send doit retourner vrai normalement donc si cela retourne faux on affiche le message d'ereur
            if (!$mail->Send()) { //Teste le return code de la fonction
                echo $mail->ErrorInfo; //Affiche le message d'erreur 
            } else {
                $_SESSION['succes'] = "Le message a bien été envoyer !";
            }
            //on ferme le IsSMTP() du tout debut
            $mail->SmtpClose();
            //on detruit lobjet mail (c'est pas obligatoire)
            unset($mail);

            include("vues/formContact.php");
            echo " OK ";
            //header('location:vues/formContact.php');
        } else {
            $_SESSION['test'] = "AAA";
            echo "Pas ok";
            // include("vues/formClient.php");
        }
        break;




    case "formulaireCreation":
        include("vues/formClient.php");
        break;

    case "formulaireContact":
        include("vues/formContact.php");
        break;

    case "formulaireConnexion":
        include("vues/formConnexion.php");
        break;

    case "creationCompte":
        $unClient = new Client();
        $nom = filter_var(securiser($_POST["nom"]), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $prenom = filter_var(securiser($_POST["prenom"]), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $mail = filter_var(securiser($_POST["mail"]), FILTER_SANITIZE_EMAIL);
        $numero = filter_var(securiser($_POST["numero"]), FILTER_SANITIZE_NUMBER_INT);
        $mdp = filter_var(securiser($_POST["mdp"]), FILTER_SANITIZE_EMAIL);
        $adresse = filter_var(securiser($_POST["adresse"]), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $codePostal = filter_var(securiser($_POST["codePostal"]), FILTER_SANITIZE_NUMBER_INT);
        $pays = filter_var(securiser($_POST["pays"]), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $token = filter_var(securiser($_POST["token"]), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        if ($token == $_SESSION["token"]) {
            $unClient->hydrate($nom, $prenom, $mail, $numero, $mdp, $adresse, $codePostal, $pays);
            Client::creationCompteClient($unClient);
            include("vues/accueil.php");
            $_SESSION['creationCompte'] = "Le compte client a bien été créer !";
            break;
        }

    case "verif":
        $mail = filter_var(securiser($_POST["mail"]), FILTER_SANITIZE_EMAIL);
        $mdp = filter_var(securiser($_POST["mdp"]), FILTER_SANITIZE_EMAIL);
        $token = filter_var(securiser($_POST["token"]), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        if ($token == $_SESSION["token"]) {
            $rep = Client::verifier($mail, $mdp);
            $_SESSION["nom"] = $rep->getNom();
            $_SESSION["prenom"] = $rep->getPrenom();
            $_SESSION["mail"] = $rep->getMail();
            $_SESSION["numero"] = $rep->getNumero();
            $_SESSION["adresse"] = $rep->getAdresse();
            $_SESSION["code_postal"] = $rep->code_postal;
            $_SESSION["pays"] = $rep->getPays();
            if ($rep == true) {
                $_SESSION["autorisationClient"] = "OK";

                if (isset($_SESSION["error"])) {
                    unset($_SESSION["error"]);
                }
                $lesProduits = Produit::afficherTous();
                include("vues/listeProduits.php");
            } else {
                $_SESSION["error"] = "Echec connexion";
                include("vues/listeProduits.php");
            }
        } else {
            echo "Pas ok";
        }
        break;

    case "deconnexion":
        Client::deconnexion();
        include("vues/accueil.php");
        break;
}
