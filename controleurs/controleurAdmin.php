<?php
$action = $_GET["action"];


switch ($action) {


    case "formulaire":
        include("vues/formAdmin.php");
        break;


    case "verif":

        $login = filter_var(securiser($_POST["login"]), FILTER_SANITIZE_ENCODED);
        $mdp = filter_var(securiser($_POST["mdp"]), FILTER_SANITIZE_ENCODED);
        $token = filter_var(securiser($_POST["token"]), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        if ($token == $_SESSION["token"]) {

            $rep = Admin::verifier($login, $mdp);

            $_SESSION["rep"] = $rep;

            if ($rep == true) {
                $_SESSION["autorisation"] = "OK";
                if (isset($_SESSION["error"])) {
                    unset($_SESSION["error"]);
                }
                $lesProduits = Produit::afficherTous();
                include("vues/accueilAdmin.php");
            } else {
                $_SESSION["error"] = "Echec connexion";
                include("vues/formAdmin.php");
            }
        } else {
            echo "Veuillez rééssayer";
            include("vues/accueil.php");
        }
        break;

    case "deconnexion":
        Admin::deconnexion();
        include("vues/accueil.php");
        break;

    case "espaceAdmin":
        $authorisation = $_SESSION["authorisation"] ?? false;
        if ($authorisation) {
            $lesProduits = Produit::afficherTous();
            include("vues/accueilAdmin.php");
        } else {
            include("vues/accueil.php");
        }
        break;
}
