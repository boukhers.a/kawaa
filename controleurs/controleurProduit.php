<?php
$action = $_GET["action"];
switch ($action) {
    case "liste":
        // permet d'afficher les produits en les triant par catégorie
        if (isset($_GET["categorieID"])) {
            $lesProduits = Produit::afficherParCategorie($_GET["categorieID"]);
            include("vues/listeProduits.php");
            break;
        }
        $lesProduits = Produit::afficherTous();
        $lesCategories = Categorie::afficherTous();
        include("vues/listeProduits.php");
        break;

    case "recherche":
        $nomRechercher = securiser($_POST["recherche"]);
        $nomRechercher = mb_strtolower($nomRechercher);
        $lesProduits = Produit::recherche($nomRechercher);
        include("vues/listeProduits.php");
        break;

    case "formAjout":
        $autorisation = $_SESSION["autorisation"] ?? false;
        if ($autorisation) {
            include("vues/formAjout.php");
        }
        break;

    case "traitementAjout":
        $token = filter_var(securiser($_POST["token"]), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        if ($token == $_SESSION["token"]) {
            $autorisation = $_SESSION["autorisation"] ?? false;
            if ($autorisation) {
                $unProduit = new Produit();
                $nom = filter_var(securiser($_POST["nom"]), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
                $prix = filter_var(securiser($_POST["prix"]), FILTER_SANITIZE_NUMBER_FLOAT);
                $description = filter_var(securiser($_POST["description"]), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
                $idCat = filter_var(securiser($_POST["categorie"]), FILTER_SANITIZE_NUMBER_INT);
                $photo = filter_var(securiser($_FILES["img"]["name"]), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
                $unProduit->hydrate($nom, $prix, $idCat, $photo, $description);
                Produit::ajouter($unProduit);
                $nom_image = basename($_FILES["img"]["name"]);
                $chemin = "images/" . $nom_image;
                move_uploaded_file($_FILES['img']["tmp_name"], $chemin);
            }
            $_SESSION['succes'] = "Le produit a été créer !";
            header('location:index.php?uc=admin&action=espaceAdmin');

            break;
        }
        break;

    case "formModif":
        $unProduit = Produit::trouverUnProduit($_GET["modif"]);
        include("vues/formModif.php");
        break;

    case "confirmerSuppression":
        $unProduit = Produit::trouverUnProduit($_GET["modif"]);
        include("vues/confirmSuppression.php");
        break;

    case "traitementSupprimer":
        $id = $_GET["id"];
        Produit::supprimerUnProduit($id);
        $_SESSION['succes'] = "Le produit a été supprimer !";
        header('location:index.php?uc=admin&action=espaceAdmin');
        break;

    case "traitementModif":

        $autorisation = $_SESSION["autorisation"] ?? false;
        if ($autorisation) {
            $unProduit = Produit::trouverUnProduit($_GET["id"]);
            Produit::modifier($unProduit);
            $_SESSION['modif'] = "Le produit a bien été modifié !";
        }
        // include("vues/accueilAdmin.php");
        header('location:index.php?uc=admin&action=espaceAdmin');

        break;

    case "ajoutPanier":
        $id = $_GET["id"];
        Produit::AjoutPanier($id);
        $lesProduits = Produit::afficherTous();
        $_SESSION['succes'] = "Le produit a été ajouté au panier !";
        include("vues/listeProduits.php");
        break;

    case "ajouterPanier2":
        $id = $_GET["id"];

        Produit::AjoutPanier2($id);
        include("vues/panier.php");
        break;

    case "retraitPanier2":
        extract($_GET);

        // $_SESSION["panier"][$id++];
        Produit::retraitPanier2($id);
        // header("vues/panier.php");

        include("vues/panier.php");
        break;
    case "afficherPanier":
        if (isset($_SESSION['panier'])) {
            $lesProduits = Produit::AfficherPanier($_SESSION["panier"]);
        }
        include("vues/panier.php");
        break;

    case "supprimerPanier":
        Produit::SupprimerPanier();
        $lesProduits = Produit::afficherTous();
        $lesCategories = Categorie::afficherTous();
        include("vues/listeProduits.php");
        break;
}
